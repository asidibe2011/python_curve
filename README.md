https://asidibe2011@bitbucket.org/asidibe2011/python_curve.git
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
xl = pd.ExcelFile("/Data_BLS3.xlsx")
df = xl.parse("Feuil1")
data=df.values
day=data[:,0]
traitement=data[:,1]
audpc=data[:,2]
indexc4=[]
indexc8=[]
indexc14=[]
index14=[]
index18=[]
index114=[]
for ii in range(len(day)):
      if (traitement[ii]=='Cont' and day[ii]==4):
         indexc4.append(ii)
      elif (traitement[ii]=='Cont' and day[ii]==8):
         indexc8.append(ii)
      elif (traitement[ii]=='Cont' and day[ii]==14):
         indexc14.append(ii)
      elif (traitement[ii]=='UV' and day[ii]==4):
         index14.append(ii)
      elif (traitement[ii]=='UV' and day[ii]==8):
         index18.append(ii)
      elif (traitement[ii]=='UV' and day[ii]==14):
         index114.append(ii)
            else:
          print ('Index n existe pas')
#Pour controle
audpc4m=np.mean(audpc[indexc4])
stdc4=np.std(audpc[indexc4])
audpc8m=np.mean(audpc[indexc8])
stdc8=np.std(audpc[indexc8])
audpc14m=np.mean(audpc[indexc14])
stdc14=np.std(audpc[indexc14])
valm=[0, audpc4m, audpc8m, audpc14m]
std=[0,stdc4,stdc8,stdc14]
#Pour uv
audpc4ma=np.mean(audpc[index14])
stdc4a=np.std(audpc[index14])
audpc8ma=np.mean(audpc[index18])
stdc8a=np.std(audpc[index18])
audpc14ma=np.mean(audpc[index114])
stdc14a=np.std(audpc[index114])
valam=[0, audpc4ma, audpc8ma, audpc14ma]
stda=[0,stdc4a,stdc8a,stdc14a]
dat=[0,4,8,14]
dat1=[0,4.1,8.1,13.9]
dat2=[0,4.2,8.2,13.8]
dat3=[0,4.3,8.3,13.7]
dat4=[0,4.4,8.4,13.6]
#figure=(4,12)
fig,ax=plt.subplots(1,1,figsize=(10,6))
plt.errorbar(dat,valm,std,color='k',linestyle='solid',label='Cont')
plt.errorbar(dat1,valam,stda,color='k',linestyle='dotted',label='UV1')
plt.errorbar(dat2,valbm,stdb,color='k',linestyle='dashed',label='UV1.5')
plt.errorbar(dat3,valcm,stdc,color='k',linestyle='dashdot',label='UV2')
plt.errorbar(dat4,valdm,stdd,color='k',linestyle=(0,(1,10)),label='UV2.75')
ax.set_xlabel('day',fontsize=15)
ax.set_ylabel('AUDPC',fontsize=15)
ax.set_xticks(dat)
ax.set_xticklabels(dat)
plt.legend(loc='best')
plt.show()


**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).